/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s14329.mpr.lab2;

import tests.AddressManagerTest;
import tests.ClientDetailsManagerTest;
import tests.OrderItemManagerTest;

/**
 *
 * @author shymek
 */
public class S14329MprLab2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        AddressManagerTest addressManagerTest = new AddressManagerTest();
        addressManagerTest.checkConnection();
        addressManagerTest.checkAdding();
        
        ClientDetailsManagerTest clientDetailsManagerTest = new ClientDetailsManagerTest();
        clientDetailsManagerTest.checkConnection();
        clientDetailsManagerTest.checkAdding();
        
        OrderItemManagerTest orderItemManagerTest = new OrderItemManagerTest();
        orderItemManagerTest.checkConnection();
        orderItemManagerTest.checkAdding();
                
    }
}