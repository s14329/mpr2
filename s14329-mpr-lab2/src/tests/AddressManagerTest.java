/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import data.Address;
import logic.AddressManager;
import static org.junit.Assert.*;
import java.util.List;

/**
 *
 * @author shymek
 */
public class AddressManagerTest {
    
    AddressManager addressManager = new AddressManager() {};
        
        private final static String STREET_1 = "Mickiewicza";
        private final static String BUILDING_NR_1 = "29";
        private final static String FLAT_NR_1 = "2";
        private final static String POSTAL_CODE_1 = "80-845";
        private final static String CITY_1 = "Gdańsk";
        private final static String COUNTRY_1 = "Polska";
        
        public void checkConnection(){
                assertNotNull(addressManager.getConnection());
        }
        
        public void checkAdding(){
            
            Address address = new Address(STREET_1, BUILDING_NR_1, FLAT_NR_1, POSTAL_CODE_1, CITY_1, COUNTRY_1);
            
            addressManager.clearAddress();
            assertEquals(1,addressManager.addAddress(address));
            
            List<Address> addresses = addressManager.getAllAddresses();
            Address addressRetrieved = addresses.get(0);
            
            assertEquals(STREET_1, addressRetrieved.getStreet());
            assertEquals(BUILDING_NR_1, addressRetrieved.getBuildingNumber());
            assertEquals(FLAT_NR_1, addressRetrieved.getFlatNumber());
            assertEquals(POSTAL_CODE_1, addressRetrieved.getPostalCode());
            assertEquals(CITY_1, addressRetrieved.getCity());
            assertEquals(COUNTRY_1, addressRetrieved.getCountry());
            
        }
    
}
