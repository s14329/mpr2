/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import data.OrderItem;
import logic.OrderItemManager;
import static org.junit.Assert.*;
import java.util.List;

/**
 *
 * @author shymek
 */
public class OrderItemManagerTest {
    
    OrderItemManager orderItemManager = new OrderItemManager();
    
    private final static String NAME_1 = "Headphones";
    private final static String DESCRIPTION_1 = "Bose QuietComfort 35";
    private final static double PRICE_1 = 349;
    
    public void checkConnection(){
        assertNotNull(orderItemManager.getConnection());
    }
    
    public void checkAdding(){
        
        OrderItem orderItem = new OrderItem(NAME_1, DESCRIPTION_1, PRICE_1);
        
        orderItemManager.clearOrderItem();
        assertEquals(1,orderItemManager.addOrderItem(orderItem));
        
        List<OrderItem> clientsDetails = orderItemManager.getAllOrderItems();
        OrderItem orderItemRetrieved = clientsDetails.get(0);
        
        assertEquals(NAME_1, orderItemRetrieved.getName());
        assertEquals(DESCRIPTION_1, orderItemRetrieved.getDescription());
        assertEquals(PRICE_1, orderItemRetrieved.getPrice());
  
    }
    
}
