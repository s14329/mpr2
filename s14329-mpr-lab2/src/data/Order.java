/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.List;

/**
 *
 * @author shymek
 */
public class Order {
    
    private long id;
    private ClientDetails client;
    private Address deliveryAddress;
    private List<OrderItem> Items;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the client
     */
    public Long getClient() {
        return client.getId();
    }

    /**
     * @param client the client to set
     */
    public void setClient(ClientDetails client) {
        this.client = client;
    }

    /**
     * @return the deliveryAddress
     */
    public Long getDeliveryAddress() {
        return deliveryAddress.getId();
    }

    /**
     * @param deliveryAddress the deliveryAddress to set
     */
    public void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    /**
     * @return the Items
     */
    public List<OrderItem> getItems() {
        return Items;
    }

    /**
     * @param Items the Items to set
     */
    public void setItems(List<OrderItem> Items) {
        this.Items = Items;
    }

    
}
